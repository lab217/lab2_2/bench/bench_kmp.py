import cProfile
import pstats
from pstats import SortKey
from src.app import util
import bench_util
import matplotlib.pyplot as plt
import numpy as np
from numpy import poly1d as np_poly1d, polyfit as np_polyfit


operation_lst = []
len_array = []

for i in range(2, 319, 1):
    lst = bench_util.generate_random_string(i)
    index = util.index
    temp = 0
    for j in range(2):
        cProfile.run('util.kmp(lst, index)', 'stats.log')
        with open('../output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
        f = open('../output.txt')
        line = bench_util.correct_lines(f)
        f.close()
        temp += int(line)
    operation_lst.append(temp//5)
    len_array.append(i)

teory_operation = list(operation_lst)
np.seterr(divide = 'ignore')
teory_hard = [ i for i in range(len(teory_operation))]
print(teory_hard)

plt.title('"kmp"')

trend = np_poly1d(np_polyfit(len_array, operation_lst, 2))
plt.plot(len_array, trend(operation_lst), c='green', label='Сортировка')
plt.plot(len_array, teory_hard, c='red', label='Теор. сложность')
plt.grid()
plt.legend()
plt.xlabel('Сложность алгоритма')
plt.ylabel('Количество операций ')


plt.show()